import java.io.*;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;
import java.io.IOException;

public class MovieRecommendation {

	public static void main(String[] args) throws IOException{
		if(args.length > 0) {
			File file1 = new File(args[0]);
			File file2 = new File(args[1]);
			File file3 = new File(args[2]);
			
			Scanner inputFile1 = new Scanner(file1);
			Scanner inputFile2 = new Scanner(file2);
			Scanner inputFile3 = new Scanner(file3);
			
			ArrayList<String> movies1 = new ArrayList<String>();
			ArrayList<String> movies2 = new ArrayList<String>();
			ArrayList<String> movies3 = new ArrayList<String>();
			
			ReadFile(file1, inputFile1, movies1);
			ReadFile(file2, inputFile2, movies2);
			ReadFile(file3, inputFile3, movies3);
			
			FindCommonMovies(movies1, movies2, movies3);
			
		}

	}
	
	public static void ReadFile(File file, Scanner input_file, ArrayList array) 
	{
		int i = 0;
		
		while(input_file.hasNext()) {
			
			String info = input_file.nextLine();
			array.add(info);
		}
		
		input_file.close();
		
	}
	
	public static String RecommendedMovie(ArrayList recommendationArray) {
		
		
		Random randomGenerator;
		randomGenerator = new Random();
		int index = randomGenerator.nextInt(recommendationArray.size());
		String final_output = recommendationArray.get(index).toString();
		
		return final_output;
	}
	
	public static void FindCommonMovies(ArrayList array1, ArrayList array2, ArrayList array3) {
		
		int maxSize = GetMaxArrayLength(array1, array2,array3);
		//System.out.println(maxSize);
		array1 = NormalizeArray(array1, maxSize);
		array2 = NormalizeArray(array2, maxSize);
		array3 = NormalizeArray(array3, maxSize);
		
		//System.out.println("Array1 size:" + array2.size()  );
		
		ArrayList<String> recommendedMovies = new ArrayList<String>(maxSize);
		
		for(int i = 0; i < maxSize; i++) {
			for(int j = 0; j < maxSize; j++) {
				if(array1.get(i).equals(array2.get(j)) && array1.get(i).toString() != " ") {
					
					recommendedMovies.add(array1.get(i).toString());
				}
					
				
			}//I could also take into consideration the duration and the number of minutes watched by the person for a more accurate recommendation but i was  unfortunately out of time.
			
			
		}
		
		//System.out.println(recommendedMovies);
		
		for(int i = 0; i < recommendedMovies.size(); i++) {
			int j = 0;
			while((!recommendedMovies.get(i).equals(array3.get(j))) && j < maxSize) {
				if(j == maxSize - 1) {
					recommendedMovies.remove(i);
					break;
				}
					
				j++;
			}
			
		}
		
		//System.out.println(recommendedMovies);
			
		System.out.println("Recommended movie: " + RecommendedMovie(recommendedMovies));
	}
	
	public static ArrayList NormalizeArray(ArrayList array, int maxSize) {
		
		int offset = 0;
		offset = maxSize - array.size();
		
		if(array.size() < maxSize) {
			for(int i = 0; i < offset;i++) {
					
				array.add(" ");
			}
		}
		return array;
	}
	
	
	public static int GetMaxArrayLength(ArrayList array1, ArrayList array2, ArrayList array3) {
		int max = 0;
		int c1, c2, c3;
		c1 = array1.size();
		c2 = array2.size();
		c3 = array3.size();
		
		
		if(max < c1)
			max = c1;
		if(max < c2)
			max = c2;
		if(max < c3)
			max = c3;
		
		return max;
		
	}

}
