//The final version of the method

import java.io.*;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;
import java.util.regex.MatchResult;
import java.io.IOException;

public class MovieRecommendation {

	public static void main(String[] args) throws IOException{
		if(args.length == 3 || args.length == 4) {
			
			Scanner sc1 = new Scanner(new File(args[0]));
			Scanner sc2 = new Scanner(new File(args[1]));
			Scanner sc3 = new Scanner(new File(args[2]));
		
			
			//its array stores the movie information(name and watch time) for every person
			ArrayList<MovieInfo> movieInfos1 = new ArrayList<MovieInfo>();
			ArrayList<MovieInfo> movieInfos2 = new ArrayList<MovieInfo>();
			ArrayList<MovieInfo> movieInfos3 = new ArrayList<MovieInfo>();
			
			ReadFile(sc1, movieInfos1);
			ReadFile(sc2, movieInfos2);
			ReadFile(sc3, movieInfos3);
			
			
			
			Recommend(movieInfos1, movieInfos2, movieInfos3);
			
		}else {
			System.err.print("usage: java MovieRecommendation file1.txt file2.txt file3.txt output.txt");
		}

	}
	
	public static void ReadFile(Scanner input_file, ArrayList array) 
	{
		int i = 0;
		String s ="";
		while(input_file.hasNextLine()) {
			
			MovieInfo mi = new MovieInfo("", 0);
			s = input_file.nextLine();
			
			Scanner sc = new Scanner(s);
			sc.findInLine("(\\d+)/(\\d+)/(\\d+) (.*) (\\d+)min (\\d+)min (\\S+)");
			MatchResult result = sc.match();
			mi.name = result.group(4);
			
			int watchTime = Integer.parseInt(result.group(6));
			mi.timeWatched = watchTime;
			
			
			array.add(mi);
		}
		
		input_file.close();
		
	}
	
	public static void Recommend(ArrayList<MovieInfo> array1, ArrayList<MovieInfo> array2, ArrayList<MovieInfo> array3) {
		FindCommonMovies(array1, array2, array3);
	}

	
//Find common movies by name and store them to a new array list	
	public static void FindCommonMovies(ArrayList<MovieInfo> array1, ArrayList<MovieInfo> array2, ArrayList<MovieInfo> array3) {
		
		
		
		ArrayList<MovieInfo> recommendedMovies = new ArrayList<MovieInfo>();
		
		for(int i = 0; i < array1.size(); i++) {
			for(int j = 0; j < array2.size(); j++) {
				if(array1.get(i).getName().equals(array2.get(j).getName())){
					
					int average_watchTime = (int)((array1.get(i).getTime() + array2.get(j).getTime())/2);
					array1.get(i).timeWatched = average_watchTime;
					recommendedMovies.add(array1.get(i));
				}
						
			}
			
		}
			
		
		int index = 0;
		boolean get_average = true;
		for(int i = 0; i < recommendedMovies.size(); i++) {
			int j = 0;
			index = 0;
			while((!recommendedMovies.get(i).getName().equals(array3.get(j).getName())) && j < array3.size()) {
				if(j == array3.size() - 1) {
					recommendedMovies.remove(i);
					get_average = false;
					index += 1;
					break;
				}
					
				j++;
			}
			if(get_average) {
				int average_timeWatched = (int)((recommendedMovies.get(i - index).getTime() + array3.get(j).getTime())/2);
				recommendedMovies.get(i - index).timeWatched = average_timeWatched;
			}
			get_average = true;
		}
		
		
		
		CompareWatchTimes(recommendedMovies);
		
	}
	//Compare the average watch times of the common movies
	public static void CompareWatchTimes(ArrayList<MovieInfo> array) {
		if(array.size() != 0) {
			int min = array.get(0).getTime();
			ArrayList<MovieInfo> finalList = new ArrayList<MovieInfo>();
			
			for(int i = 0; i < array.size(); i++) {
				if(i == 0)
					min = array.get(0).getTime();
				if(min > array.get(i).getTime()) {
					min = array.get(i).getTime();
					finalList.clear();
					finalList.add(array.get(i));
				}
				else if(min == array.get(i).getTime())
					finalList.add(array.get(i));
						
			}
			if(finalList.size() > 1) //if more than one movies have the same average watch time
				System.out.println("Recommended movie: " + PickRandomMovie(finalList)); 
			else	
				System.out.println("Recommended movie: " + finalList.get(0).getName());
		}
		else
			System.out.println("Could not find common movies");
		
}
	//Pick a random movie in case all common movies have the same average watch time 
	public static String PickRandomMovie(ArrayList<MovieInfo> array) { 
		Random randomGenerator;
		randomGenerator = new Random();
		int index = randomGenerator.nextInt(array.size());
		String final_output = array.get(index).getName();
		
		return final_output;
	}
	
	
	
	
	

}

class MovieInfo{  //Contains the name and the watch time of every movie
	
	String name;
	int timeWatched;
	
	MovieInfo(String name, int timeWatched){
		this.name = name;
		this.timeWatched = timeWatched;
		
	}
	
	public String getName() {
		return name;
	}
	
	public int getTime() {
		return timeWatched;
	}

	
}